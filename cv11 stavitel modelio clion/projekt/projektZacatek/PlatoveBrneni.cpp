#include "PlatoveBrneni.h"

PlatoveBrneni::PlatoveBrneni(int vaha, int odolnost, int velikostPlatu):Brneni(vaha,odolnost) {
	m_velikostplatu = velikostPlatu;
}

int PlatoveBrneni::getBonusUtoku() {
	return m_odolnost/2;
}

int PlatoveBrneni::getBonusObrany() {
	return m_odolnost;
}

void PlatoveBrneni::printInfo() {
	Brneni::printInfo();
    cout << "-typ: platove brneni" << endl;
    cout << "-velikostPlatu: " << m_velikostplatu << endl;
}
