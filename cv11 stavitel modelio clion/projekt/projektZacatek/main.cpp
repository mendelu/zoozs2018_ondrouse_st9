#include <iostream>
#include "Rytir.h"
#include "LehkaUtocnaHelma.h"
#include "KrouzkoveBrneni.h"


using namespace std;

int main()
{
    Rytir* r1 = new Rytir("Artus",25);
    LehkaUtocnaHelma* lum = new LehkaUtocnaHelma(3);
    r1->setHelma( lum );
    KrouzkoveBrneni* kb = new KrouzkoveBrneni(10,5,1);
    r1->setZbroj( kb );
    r1->print();

    delete r1;
    delete lum;
    delete kb;

    return 0;
}
