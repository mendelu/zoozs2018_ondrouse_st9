#include "Helma.h"

Helma::Helma(int velikost) {
	m_velikost = velikost;
}

void Helma::printInfo() {
	cout << "helma:" << endl;
	cout << "-velikost helmy: " << m_velikost << endl;
	cout << "-bonus obrany: " << getBonusObrany() << endl;
}
