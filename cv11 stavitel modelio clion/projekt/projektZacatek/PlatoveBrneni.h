#ifndef PlatoveBrneni_H
#define PlatoveBrneni_H

#include "Brneni.h"


	class PlatoveBrneni : public Brneni {

	public:
		int m_velikostplatu;

		PlatoveBrneni(int vaha, int odolnost, int velikostPlatu);

		int getBonusUtoku();

		int getBonusObrany();

		void printInfo();
	};

#endif // PlatoveBrneni_H
