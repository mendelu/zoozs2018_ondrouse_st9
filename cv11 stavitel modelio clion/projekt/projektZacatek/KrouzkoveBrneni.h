#ifndef KrouzkoveBrneni_H
#define KrouzkoveBrneni_H

#include "Brneni.h"


	class KrouzkoveBrneni : public Brneni {

	public:
		int m_ohebnost;

		KrouzkoveBrneni(int vaha, int odolnost, int ohebnost);

		int getBonusObrany();

		int getBonusUtoku();

		void printInfo();
	};

#endif // KrouzkoveBrneni_H
