#include "Brneni.h"

Brneni::Brneni(int vaha, int odolnost) {
	m_vaha = vaha;
	m_odolnost = odolnost;
}

void Brneni::printInfo() {
	cout << "brneni: " << endl;
	cout << "-vaha: " << m_vaha << endl;
	cout << "-odolnost: " << m_odolnost << endl;
	cout << "-bonus utoku: " << getBonusUtoku() << endl;
	cout << "-bonus obrany: " << getBonusObrany() << endl;
}
