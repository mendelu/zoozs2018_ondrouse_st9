//
// Created by ondrouse on 05.12.2018.
//

#include "RytirBuilder.h"

void RytirBuilder::CreateRytir(string jmeno, int sila)
{
    m_rytir = new Rytir(jmeno, sila);
}

Rytir* RytirBuilder::GetRytir()
{
    return  m_rytir;
}