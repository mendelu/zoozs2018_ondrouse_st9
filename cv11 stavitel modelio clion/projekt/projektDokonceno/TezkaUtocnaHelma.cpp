#include "TezkaUtocnaHelma.h"

TezkaUtocnaHelma::TezkaUtocnaHelma(int velikost):Helma(velikost) {

}

int TezkaUtocnaHelma::getBonusObrany() {
	return m_velikost;
}

void TezkaUtocnaHelma::printInfo() {
	Helma::printInfo();
    cout << "-typ: tezka obrana helma" << endl;
}
