#ifndef Brneni_H
#define Brneni_H

#include <iostream>
using namespace std;


	class Brneni {

	private:
		int m_vaha;
    protected:
		int m_odolnost;

	public:
		Brneni(int vaha, int odolnost);

		virtual int getBonusUtoku() = 0;

		virtual int getBonusObrany() = 0;

		void printInfo();
	};

#endif // Brneni_H
