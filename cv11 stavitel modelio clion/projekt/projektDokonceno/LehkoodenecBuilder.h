//
// Created by ondrouse on 05.12.2018.
//

#ifndef PROJEKTZACATEK_LEHKOODENECBUILDER_H
#define PROJEKTZACATEK_LEHKOODENECBUILDER_H


#include "RytirBuilder.h"

class LehkoodenecBuilder : public RytirBuilder
{
    void BuildZbroj(int vaha, int odolnost, int ohebnost, int velikostPlatu);
    void BuildHelma(int velikostHelmy);
};


#endif //PROJEKTZACATEK_LEHKOODENECBUILDER_H
