#include <iostream>
#include "Rytir.h"
#include "LehkaUtocnaHelma.h"
#include "KrouzkoveBrneni.h"
#include "RytirDirector.h"
#include "LehkoodenecBuilder.h"


using namespace std;

int main()
{
    Rytir* r1 = new Rytir("Artus",25);
    LehkaUtocnaHelma* lum = new LehkaUtocnaHelma(3);
    r1->setHelma( lum );
    KrouzkoveBrneni* kb = new KrouzkoveBrneni(10,5,1);
    r1->setZbroj( kb );
    r1->print();

    delete r1;
    delete lum;
    delete kb;

    RytirDirector* reditel;
    LehkoodenecBuilder* lb = new LehkoodenecBuilder();
    reditel = new RytirDirector(lb);
    Rytir* r5 = reditel->GetRytir(80,10,20,50,3,"Artus",50);
    Rytir* r2 = reditel->GetRytir(95,10,20,50,3,"Artus",50);
    Rytir* r3 = reditel->GetRytir(100,10,20,50,3,"Artus",50);
    Rytir* r4 = reditel->GetRytir(120,10,20,50,3,"Artus",50);

    r5->print();

    return 0;
}
