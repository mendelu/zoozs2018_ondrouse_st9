cmake_minimum_required(VERSION 3.12)
project(projektZacatek)

set(CMAKE_CXX_STANDARD 14)

include_directories(.)

add_executable(projektZacatek
        Brneni.cpp
        Brneni.h
        Helma.cpp
        Helma.h
        KrouzkoveBrneni.cpp
        KrouzkoveBrneni.h
        LehkaUtocnaHelma.cpp
        LehkaUtocnaHelma.h
        main.cpp
        PlatoveBrneni.cpp
        PlatoveBrneni.h
        Rytir.cpp
        Rytir.h
        TezkaUtocnaHelma.cpp
        TezkaUtocnaHelma.h RytirBuilder.cpp RytirBuilder.h LehkoodenecBuilder.cpp LehkoodenecBuilder.h RytirDirector.cpp RytirDirector.h)
