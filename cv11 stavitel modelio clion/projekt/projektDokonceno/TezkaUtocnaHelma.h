#ifndef TezkaUtocnaHelma_H
#define TezkaUtocnaHelma_H

#include "Helma.h"


	class TezkaUtocnaHelma : public Helma {


	public:
		TezkaUtocnaHelma(int velikost);

		int getBonusObrany();

		void printInfo();
	};

#endif
