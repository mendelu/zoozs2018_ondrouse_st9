//
// Created by ondrouse on 05.12.2018.
//

#ifndef PROJEKTZACATEK_RYTIRDIRECTOR_H
#define PROJEKTZACATEK_RYTIRDIRECTOR_H


#include "RytirBuilder.h"

class RytirDirector
{
private:
    RytirBuilder* m_builder;
public:
    RytirDirector(RytirBuilder* builder);
    void SetBuilder(RytirBuilder* builder);
    Rytir* GetRytir(int vaha, int odolnost, int ohebnost, int velikostPlatu, int velikostHelmy, string jmeno, int sila);
};


#endif //PROJEKTZACATEK_RYTIRDIRECTOR_H
