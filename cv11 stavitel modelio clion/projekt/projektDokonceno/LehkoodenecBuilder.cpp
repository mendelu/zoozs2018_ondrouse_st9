//
// Created by ondrouse on 05.12.2018.
//

#include "LehkoodenecBuilder.h"
#include "KrouzkoveBrneni.h"
#include "LehkaUtocnaHelma.h"

void LehkoodenecBuilder::BuildZbroj(int vaha, int odolnost, int ohebnost, int velikostPlatu)
{
    KrouzkoveBrneni* nove = new KrouzkoveBrneni(vaha, odolnost, ohebnost);
    m_rytir->setZbroj(nove);
}

void LehkoodenecBuilder::BuildHelma(int velikostHelmy)
{
    LehkaUtocnaHelma* nova = new LehkaUtocnaHelma(velikostHelmy);
    m_rytir->setHelma(nova);
}