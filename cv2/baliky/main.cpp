#include <iostream>
using namespace std;

class Balik
{
public:
    string m_odesilatel = "";
    string m_prijemce = "";
    float m_vaha = 0.0f;
    int m_dobirka = 0;

    Balik(string odesilatel, string prijemce, float vaha, int dobirka)
    {
        m_odesilatel = odesilatel;
        m_prijemce = prijemce;
        m_vaha = vaha;
        m_dobirka = dobirka;
    }

    Balik(string odesilatel, string prijemce)
    {
        m_odesilatel = odesilatel;
        m_prijemce = prijemce;
        m_vaha = 0.0f;
        m_dobirka = 0;
    }

    Balik(string odesilatel, string prijemce, float vaha)
    {
        m_odesilatel = odesilatel;
        m_prijemce = prijemce;
        m_vaha = vaha;
        m_dobirka = 0;
    }

    void SetVaha(float vaha)
    {
        m_vaha = vaha;
    }

    void PrintInfo()
    {
        cout << "----------" << endl;
        cout << "Odesilatel: " << m_odesilatel << endl;
        cout << "Prijemce: " << m_prijemce << endl;
        cout << "Vaha: " << m_vaha << endl;
        cout << "Dobirka: " << m_dobirka << endl;
        cout << "----------" << endl;
    }
};

int main() {
    Balik* balicek = new Balik("Adam","Eva",1.1f,150);
    balicek->SetVaha(1.5f);
    balicek->PrintInfo();
    delete balicek;
    return 0;
}