#include <iostream>
#include "Kun.h"
#include "ZavodniKun.h"
#include "ChovnyKun.h"
#include "Konirna.h"

int main() {
    //Kun* k = new Kun("Elis",255000,500);
    //k->PrintInfo();
    //delete k;

    ZavodniKun* zk = new ZavodniKun("Dharma",450000,450,3);
    //zk->PrintInfo();
    ChovnyKun* ch = new ChovnyKun("Francesco",333000,350,1);
    //ch->PrintInfo();
    Kun* k2 = new ZavodniKun("AlanZ",222111,480,5);
    //k2->PrintInfo();
    k2->GetProdejniCena();
    Kun* k3 = new ChovnyKun("AlanCh",222111,480,1);
    //k3->PrintInfo();
    k3->GetProdejniCena();

    Konirna* rancUPodkovy = new Konirna(2500000);
    rancUPodkovy->KupKone(zk);
    rancUPodkovy->KupKone(ch);
    rancUPodkovy->KupKone(k2);
    rancUPodkovy->KupKone(k3);

    //rancUPodkovy->ProdejKone("Dharma");
    //rancUPodkovy->PrintInfo();

    Konirna* rancNaLouce = new Konirna(1000111);
    Kun* pom = rancUPodkovy->ProdejKone("Dharma");
    rancNaLouce->KupKone( pom );
    rancNaLouce->PrintInfo();

    delete rancUPodkovy;
    delete rancNaLouce;
    delete zk;
    delete ch;
    delete k2;
    delete k3;
    return 0;
}