#ifndef CVKONIRNA_ZAVODNIKUN_H
#define CVKONIRNA_ZAVODNIKUN_H

#include <iostream>
#include "Kun.h"

using namespace std;

class ZavodniKun : public Kun
{
private:
    int m_pocetVyhranych;
public:
    ZavodniKun(string jmeno, int cena, int vaha, int pocetVyher);
    void SetPocetVyhranych(int pocet);
    int GetProdejniCena();
    void PrintInfo();

};


#endif //CVKONIRNA_ZAVODNIKUN_H
