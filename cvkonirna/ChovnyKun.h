#ifndef CVKONIRNA_CHOVNYKUN_H
#define CVKONIRNA_CHOVNYKUN_H


#include "Kun.h"

class ChovnyKun : public Kun
{
private:
    int m_stari;
public:
    ChovnyKun(string jmeno, int cena, int vaha,int stari);
    void SetStari(int vek);
    void PrintInfo();
    int GetProdejniCena();
};


#endif //CVKONIRNA_CHOVNYKUN_H
