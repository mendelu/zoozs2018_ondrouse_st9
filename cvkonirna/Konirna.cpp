#include "Konirna.h"

Konirna::Konirna(int penize)
{
    m_stavPokladny = penize;
}

void Konirna::PrintInfo()
{
    for (int i=0; i<m_stado.size(); i++)
    {
        m_stado.at(i)->PrintInfo();
    }
}

bool Konirna::KupKone(Kun* koho)
{
    int prodej = koho->GetProdejniCena();
    if(m_stavPokladny < prodej)
    {
        return false;
    }
    m_stado.push_back(koho);
    cout << "nakoupeno" << endl;
    return true;
}

Kun* Konirna::ProdejKone(string jmeno)
{
    Kun* who = nullptr;
    for (int i=0; i<m_stado.size(); i++)
    {
        who = m_stado.at(i);
        if (who->Getjmeno()==jmeno)
        {
            m_stavPokladny += who->GetProdejniCena();
            m_stado.erase(m_stado.begin()+i);
            return who;
        }
    }
    return nullptr;
}
