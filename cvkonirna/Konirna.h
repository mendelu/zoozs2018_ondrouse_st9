#ifndef CVKONIRNA_KONIRNA_H
#define CVKONIRNA_KONIRNA_H


#include <vector>
#include "Kun.h"

class Konirna {
private:
    int m_stavPokladny;
    vector<Kun*> m_stado;
public:
    Konirna(int penize);
    bool KupKone(Kun* koho);
    Kun* ProdejKone(string jmeno);
    void PrintInfo();
};


#endif //CVKONIRNA_KONIRNA_H
