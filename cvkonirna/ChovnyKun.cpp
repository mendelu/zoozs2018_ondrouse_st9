#include "ChovnyKun.h"

ChovnyKun::ChovnyKun(string jmeno, int cena, int vaha, int stari)
    :Kun(jmeno,cena,vaha)
{
    m_stari = stari;
}

void ChovnyKun::SetStari(int vek)
{
    m_stari = vek;
}

int ChovnyKun::GetProdejniCena()
{
    return m_zakladniCena - 10000*m_stari;
}

void ChovnyKun::PrintInfo()
{
    cout << "--Chovny Kun--" << endl;
    Kun::PrintInfo();
    cout << "prodejni cena: " << GetProdejniCena() << endl;
    cout << "vek: " << m_stari << endl;
    cout << "---------------" << endl;
}