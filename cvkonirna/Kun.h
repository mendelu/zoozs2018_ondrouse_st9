#ifndef CVKONIRNA_KUN_H
#define CVKONIRNA_KUN_H

#include <iostream>
using namespace std;

class Kun {

private:
    string m_jmeno;
    int m_vaha;
protected:
    int m_zakladniCena;
public:
    Kun(string jmeno, int cena, int vaha);
    void SetVaha(int vaha);
    string Getjmeno();
    virtual void PrintInfo();
    virtual int GetProdejniCena()=0;
};


#endif //CVKONIRNA_KUN_H
