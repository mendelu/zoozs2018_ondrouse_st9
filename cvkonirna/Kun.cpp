#include "Kun.h"

Kun::Kun(string jmeno, int cena, int vaha)
{
    m_jmeno = jmeno;
    m_vaha = vaha;
    m_zakladniCena = cena;
}

void Kun::SetVaha(int vaha)
{
    m_vaha = vaha;
}

void Kun::PrintInfo()
{
    cout << "jmeno: " << m_jmeno << endl;
    cout << "vaha: " << m_vaha << endl;
    cout << "zakl. cena: " << m_zakladniCena << endl;
}

string Kun::Getjmeno()
{
    return m_jmeno;
}

