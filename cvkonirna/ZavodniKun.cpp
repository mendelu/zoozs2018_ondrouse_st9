#include "ZavodniKun.h"

ZavodniKun::ZavodniKun(string jmeno, int cena, int vaha, int pocetVyher)
    :Kun(jmeno,cena,vaha)
{
    m_pocetVyhranych = pocetVyher;
}

void ZavodniKun::SetPocetVyhranych(int pocet)
{
    m_pocetVyhranych = pocet;
}

int ZavodniKun::GetProdejniCena()
{
    return m_zakladniCena + 10000*m_pocetVyhranych;
}

void ZavodniKun::PrintInfo()
{
    cout << "--Zavodni Kun--" << endl;
    Kun::PrintInfo();
    cout << "prodejni cena: " << GetProdejniCena() << endl;
    cout << "pocet vyher: " << m_pocetVyhranych << endl;
    cout << "---------------" << endl;
}