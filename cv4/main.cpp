#include <iostream>
using namespace std;

class ErrorLog
{
private:
    static ErrorLog* s_log;

    string m_errors;

    ErrorLog()
    {
        m_errors += "nasbirane logy: \n";
    }
public:
    static ErrorLog* GetLogerObject()
    {
        if (s_log == nullptr)
        {
            s_log = new ErrorLog();
        }
        return s_log;
    }

    void LogIt(string kde, string co)
    {
        m_errors += "- " + kde + ": " + co + "\n";
    }

    string GetLog()
    {
        return m_errors;
    }

};

class Zviratko
{
private:
    int m_vaha =0;
    int m_kalorie = 0;
public:
    Zviratko(int vaha, int kalorie)
    {
        m_vaha = vaha;
        m_kalorie = kalorie;
    }

    int GetVaha()
    {
        int pom = m_vaha;
        m_vaha = 0;
        return pom;
    }

    int GetKalorie()
    {
        int pom = m_kalorie;
        m_kalorie = 0;
        return pom;
    }

    void PrintInfo()
    {
        cout << "--ZVIRATKO--" << endl;
        cout << "vaha: " << m_vaha << endl;
        cout << "kalorie: " << m_kalorie << endl;
        cout << "------------" << endl;
    }
};

class Drak
{
private:
    int m_sezranaHmotnost = 0;
    int m_sezranekalorie = 0;
public:
    void Sezer(Zviratko* zv)
    {
        m_sezranaHmotnost = m_sezranaHmotnost + zv->GetVaha();
        m_sezranekalorie += zv->GetKalorie();

        ErrorLog* pom = ErrorLog::GetLogerObject();
        pom->LogIt("metoda sezer","dalsi zvire");
    }

    void PrintInfo()
    {
        cout << "DRAK sezral: " << m_sezranekalorie << "kJ a " << m_sezranaHmotnost << " g" << endl;
    }
};

ErrorLog* ErrorLog::s_log = nullptr;

int main() {
    Zviratko* tweety = new Zviratko(150,25);
    Zviratko* kacerDonald = new Zviratko(450,100);
    Drak* smak = new Drak();
    smak->Sezer(tweety);
    smak->Sezer(kacerDonald);
    smak->PrintInfo();
    tweety->PrintInfo();
    kacerDonald->PrintInfo();
    delete tweety;
    delete kacerDonald;
    delete smak;
    ErrorLog* pom = ErrorLog::GetLogerObject();
    cout << pom->GetLog() << endl;
    return 0;
}