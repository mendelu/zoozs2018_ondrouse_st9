#include <iostream>
#include <array>
using namespace std;

class Kontejner
{
private:
    string m_majitel;
    string m_obsah;
    int m_vaha;
public:
    Kontejner(int vaha, string majitel, string obsah)
    {
        m_majitel = majitel;
        m_obsah = obsah;
        m_vaha = vaha;
    }

    string GetObsah()
    {
        return m_obsah;
    }

    string GetMajitel()
    {
        return m_majitel;
    }

    int GetVaha()
    {
        return m_vaha;
    }

    void PrintInfo()
    {
        cout << "kontejner: " << m_majitel << m_obsah << m_vaha << endl;
    }
};

class Patro
{
private:
    string m_oznaceni;
    array<Kontejner*,10> m_pozice;
public:
    Patro(string oznaceni)
    {
        m_oznaceni = oznaceni;
        for(Kontejner* &k:m_pozice)
        {
            k = nullptr;
        }
    }

    void VypisObsah()
    {
        for(Kontejner* k:m_pozice)
        {
            if (k!= nullptr)
            {
                k->PrintInfo();
            }
        }
    }

    void PridejDoSkladu(int kam, Kontejner* co)
    {
        if (m_pozice.at(kam) == nullptr)
        {
            m_pozice.at(kam) = co;
        }
        else
        {
            cout << "nelze,pozice je obsazena" << endl;
        }
    }
};

class Sklad
{
private:
    array<Patro*,5> m_patra;
public:
    Sklad()
    {
        int poradoveCislo = 0;
        for(Patro* &p:m_patra)
        {
            string oznaceni = "Patro" + to_string(poradoveCislo);
            poradoveCislo += 1;
            p = new Patro(oznaceni);
        }
    }

    ~Sklad()
    {
        for (Patro *&p:m_patra)
        {
            delete p;
        }
    }

    void VypisObsah()
    {
        cout << "\nObsah skladu: " << endl;

        for(Patro* p:m_patra){
            if (p != nullptr){
                p->VypisObsah();
            }
        }
    }
};

int main() {
    Kontejner* k1 = new Kontejner(500,"Adam","kolo");
    Kontejner* k2 = new Kontejner(250,"Bara","auto");
    Kontejner* k3 = new Kontejner(150,"Cyril","pocitac");

    Patro* p = new Patro("p1");
    p->PridejDoSkladu(0,k1);
    p->PridejDoSkladu(1,k2);
    p->PridejDoSkladu(2,k2);
    p->VypisObsah();

    delete p;
    delete k1;
    delete k2;
    delete k3;
    return 0;
}