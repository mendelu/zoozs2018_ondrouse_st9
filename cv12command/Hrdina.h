//
// Created by ondrouse on 12.12.2018.
//

#ifndef CV12COMMAND_HRDINA_H
#define CV12COMMAND_HRDINA_H

#include "Prikaz.h"
#include "Lektvar.h"
#include <vector>
#include <iostream>
using namespace std;

class Hrdina
{
private:
    int m_zivot;
    vector<Lektvar*> m_lektvary;
    vector<Prikaz*> m_prikazy;
    void VypisLektvary();
    void VypisPrikazy();
public:
    Hrdina(int zivot);
    void ZvysZivot(int okolik);
    void PrintInfo();
    void Seberlektvar(Lektvar* lektvarek);
    void PouzijLektvar();
    void NaucSePrikaz(Prikaz* prikaz);
};



#endif //CV12COMMAND_HRDINA_H
