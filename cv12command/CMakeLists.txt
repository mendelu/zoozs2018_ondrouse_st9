cmake_minimum_required(VERSION 3.12)
project(cv12command)

set(CMAKE_CXX_STANDARD 14)

add_executable(cv12command main.cpp Hrdina.cpp Hrdina.h Lektvar.cpp Lektvar.h Prikaz.cpp Prikaz.h)