//
// Created by ondrouse on 12.12.2018.
//

#include "Hrdina.h"

Hrdina::Hrdina(int zivot)
{
    m_zivot =zivot;
}

void Hrdina::PrintInfo()
{
    cout<<"Hrdina: " << m_zivot;
}

void Hrdina::NaucSePrikaz(Prikaz* prikaz)
{
    m_prikazy.push_back(prikaz);
}

void Hrdina::Seberlektvar(Lektvar* lektvarek)
{
    m_lektvary.push_back(lektvarek);
}

void Hrdina::ZvysZivot(int okolik)
{
    m_zivot+=okolik;
}

void Hrdina::VypisLektvary()
{
    cout << "Lktvary: " << endl;
    for(int i=0; i<m_lektvary.size(); i++)
    {
        cout << i << ". "<< m_lektvary.at(i)->GetPopis() << endl;
    }
}

void Hrdina::VypisPrikazy()
{
    cout << "Prikazy: " << endl;
    for(int i=0; i<m_prikazy.size(); i++)
    {
        cout << i << ". "<< m_prikazy.at(i)->GetPopis() << endl;
    }
}

void Hrdina::PouzijLektvar()
{
    int kteryLektvar = 0;
    VypisLektvary();
    cout << "Ktery chces pouzit lektvar: ";
    cin >> kteryLektvar;

    int kteryPrikaz = 0;
    VypisPrikazy();
    cout << "Ktery chces pouzit prikaz: ";
    cin >> kteryPrikaz;

    m_prikazy.at(kteryPrikaz)->PouzijLektvar(this,m_lektvary.at(kteryLektvar));

    if (m_lektvary.at(kteryLektvar)->GetBobusZivota() == 0)
    {
        m_lektvary.erase(m_lektvary.begin()+kteryLektvar);
    }
}

