//
// Created by Mikulas Muron on 26/11/2018.
//

#include "PrezencniStudent.h"

PrezencniStudent::PrezencniStudent(string jmeno, OborStudia obor, int studujeLet, int prumer) : Student(jmeno, obor, studujeLet, prumer){

}

int PrezencniStudent::getStipendium() {
    return  (1.0 / getPrumer()) * PrezencniStudent::s_zakladStipendium;
}

bool PrezencniStudent::prodluzujeStudium() {
    if(getPocetLetStudia() > PrezencniStudent::s_pocetLetStudia){
        return true;
    }
    return false;
}

void PrezencniStudent::printInfo() {
    std::cout << "Prezencni student " << getJmeno() << " se stipendiem " << getStipendium() << std::endl;
}