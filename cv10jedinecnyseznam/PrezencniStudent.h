//
// Created by Mikulas Muron on 26/11/2018.
//

#ifndef CV10_PREZENCNISTUDENT_H
#define CV10_PREZENCNISTUDENT_H

#include "Student.h"

class PrezencniStudent : public Student {

protected:
    const static int s_zakladStipendium = 10000;

public:
    const static int s_pocetLetStudia = 3;

    PrezencniStudent(string jmeno, OborStudia obor, int studujeLet, int prumer);

    // slibil jsem predkovi
    int getStipendium();
    bool prodluzujeStudium();
    void printInfo();

};


#endif //CV10_PREZENCNISTUDENT_H
