//
// Created by Mikulas Muron on 26/11/2018.
//

#ifndef CV10_SEZNAMSTUDENTU_H
#define CV10_SEZNAMSTUDENTU_H

#include <iostream>
#include <vector>
#include "Student.h"


class SeznamStudentu {
private:
    static std::vector<Student*> s_studenti;
public:
    static void pridej(Student* novyStudent);
    static void pridej(string jmeno, int dobaStudia, OborStudia obor, int dojezdovaVzdalenost);
    static std::vector<Student*> najdiPodleJmena(std::string jmeno);
    static std::vector<Student*> najdiPodleOboru(OborStudia obor);
    static std::vector<Student*> getSeznam();
    static int kolikProdluzujeStudium();
    static int celkovaSumaStipendii();
    static void printInfo();
};


#endif //CV10_SEZNAMSTUDENTU_H
