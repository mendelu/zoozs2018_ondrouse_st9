//
// Created by Mikulas Muron on 26/11/2018.
//

#ifndef CV10_KOMBINOVANYSTUDENT_H
#define CV10_KOMBINOVANYSTUDENT_H

#include "Student.h"

class KombinovanyStudent : public Student {
protected:
    int m_dojezdovaVzdalenost;

public:
    const static int s_pocetLetStudia = 5;

    KombinovanyStudent(string jmeno, OborStudia obor, int studujeLet, int prumer, int dojezdovaVzdalenost);
    int getDojezdovaVzdalenost();

    // slibil jsem predkovi
    int getStipendium();
    bool prodluzujeStudium();
    void printInfo();




};


#endif //CV10_KOMBINOVANYSTUDENT_H
