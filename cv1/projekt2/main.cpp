#include <iostream>
using namespace std;

class Student
{
public:
    string m_jmeno = "";
    int m_vek = 0;
    float m_prumer = 1.0f;
    bool m_stipendium = false;

    void SetJmeno(string noveJmeno)
    {
        m_jmeno = noveJmeno;
    }

    void SetVek(int novyVek)
    {
        m_vek = novyVek;
    }

    int GetVek()
    {
        return m_vek;
    }

    bool GetStipendium()
    {
        return m_stipendium;
    }

    void PrintInfo()
    {
        cout << "Jmeno studenta: " << m_jmeno << endl;
        cout << "Prumer znamek: " << m_prumer << endl;
        cout << "Stipko: " << m_stipendium << endl;
        cout << "Vek studenta: " << m_vek << endl;
    }

};

int main() {
    Student* s1 = new Student();
    s1->SetJmeno("Bara");
    s1->SetVek(20);
    s1->PrintInfo();

    cout << "Jmeno studenta je " << s1->m_jmeno << endl;
    cout << "Vek studenta je " << s1->GetVek() << endl;
    cout << "Stipko: " << s1->GetStipendium() << endl;

    return 0;
}