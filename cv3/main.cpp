#include <iostream>
using namespace std;

class Auto
{
private:
    int m_ujeteKm = 0;
    string m_posledniRidic = "";
    int m_zbytkovaHodnota = 0;
    int m_amortizacenaKm = 20;

    void nastavZbytkovouHodnotu(int posledniJizda)
    {
        float koef = (float)m_ujeteKm / 100000;
        m_zbytkovaHodnota = m_zbytkovaHodnota - (posledniJizda * m_amortizacenaKm * koef);
    }

public:
    Auto(int nakupniCena, int amortizacenaKm)
    {
        m_ujeteKm = 0;
        m_posledniRidic = "";
        m_zbytkovaHodnota = nakupniCena;
        m_amortizacenaKm = amortizacenaKm;
    }

    Auto(int nakupniCena)
    {
        m_ujeteKm = 0;
        m_posledniRidic = "";
        m_zbytkovaHodnota = nakupniCena;
        m_amortizacenaKm = 20;
    }

    void PridejJizdu(string posledniRidic, int kolikUjel)
    {
        m_posledniRidic = posledniRidic;
        m_ujeteKm = m_ujeteKm + kolikUjel; //m_ujeteKm += kolikUjel;
        nastavZbytkovouHodnotu(kolikUjel);
    }

    void PrintInfo()
    {
        cout << "--AUTO--" << endl;
        cout << "posledni ridic: " <<  m_posledniRidic << endl;
        cout << "ujete km celkem: " << m_ujeteKm << endl;
        cout << "zbytkova hodnota: " << m_zbytkovaHodnota << endl;
        cout << "--------" << endl;
    }

    bool JeAmortizovano()
    {
        if (m_zbytkovaHodnota<0)
        {
            return true;
        }
        else
        {
            return false;
        }
        //return (m_zbytkovaHodnota<0)
    }

    string GetPosledniRidic()
    {
        return m_posledniRidic;
    }

};

int main() {
    Auto* a = new Auto(500000,33123);
    a->PridejJizdu("Pepa",150000);
    a->PrintInfo();
    cout << a->JeAmortizovano() << endl;
    delete a;

    return 0;
}